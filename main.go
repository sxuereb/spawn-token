package main

import (
	"fmt"
	"os"
	"os/exec"
	"runtime/trace"
	"sync"
)

func main() {
	traceFile, err := os.Create("trace.out")
	if err != nil {
		fmt.Println("Error creating trace file:", err)
		return
	}
	defer traceFile.Close()

	err = trace.Start(traceFile)
	if err != nil {
		fmt.Println("Error starting trace:", err)
		return
	}
	defer trace.Stop()

	var wg sync.WaitGroup
	for i := 0; i < 50; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			cmd := exec.Command("git", "-C", "./gitaly", "show", "HEAD")
			if err := cmd.Start(); err != nil {
				fmt.Fprintf(os.Stderr, "failed to start: %v", err)
			}
			cmd.Wait()
		}()
	}
	wg.Wait()
}
