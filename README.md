# Test fork lcok

<https://gitlab.com/gitlab-org/gitaly/-/issues/5327>

## Macos Setup

```shell
cat lima-arm64.yml | limactl start --name=spawn-token -
lima shell spawn-token
cd /app
go build
```

## Running tests

```shell
time ./shell-fork-git.sh

time ./spawn-token

go tool trace trace.out
```
